package Examen_isp_subiecte.s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main extends JFrame {
    JButton button;
    JTextField textField;
    JTextArea text2;
    int counter = 0;

    Main() {
        setTitle("Subiect 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 200);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 100;
        int height = 20;

        text2 = new JTextArea("f.txt");
        text2.setBounds(50,50,width,height);

        button = new JButton("Push");
        button.setBounds(50, 30, width, height);
        button.addActionListener((ActionListener) new ButtonAct());

        textField = new JTextField();
        textField.setBounds(50, 70, width, height);

        add(button);
        add(textField);
        add(text2);
    }

    class ButtonAct implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            File file = new File(text2.getText());

            try (FileReader fr = new FileReader(file))
            {
                int content;
                counter=0;
                while ((content = fr.read()) != -1) {
                    counter++;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            textField.setText(""+counter);
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
